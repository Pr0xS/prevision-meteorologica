# Previsión  meteorológica

Simple programa escrito en c que permite leer datos meteorológicos de un fichero .csv y mostrar la previsión meteorológica.

## Compilación
En la carpeta src se encuentra un archivo makefile que facilitará la compilación del programa

Simplemente ejecutando el siguiente comando deberiamos tener listo un programa llamado main listo para ejecutarse
```bash
make; make clean
```

## Documentación
En la carpeta docs se encuentra la documentación generada por doxygen. Podemos encontrar 3 carpetas

    * html
    * latex
    * pdf

En html se encuentra la versión web de la documentación, para verla simplemente deberemos abrir el archivo index.html en cualquier navegador.

En latex se encuentra los fichero latex necesarios para crear la documentacion en pdf.

En pdf se encuentra la versión pdf de la documentación
